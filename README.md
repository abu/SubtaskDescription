# SubTask Description
Kanboard plugin to add a markdown capable description field to subtasks.  
Based on  Shaun-Fong/SubtaskDescription on GitHub.

## Authors & Contributors
- Shaun Fong - Original Author
- [Alfred Bühler](https://codeberg.org/abu) - Author, Maintainer

## License
- This project is distributed under the [MIT License](https://choosealicense.com/licenses/mit/ "Read The MIT license")

## Requirements
- Kanboard >= 1.0.34

## Installation
1. Install the plugin from the Kanboard plugin manager in one click
2. Download the zip file and decompress everything under the directory `plugins/SubtaskDescription`
3. Clone this repository into the folder `plugins/SubtaskDescription`

Note: Plugin folder is case-sensitive.
