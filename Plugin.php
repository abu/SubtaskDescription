<?php

namespace Kanboard\Plugin\SubtaskDescription;

use Kanboard\Core\Plugin\Base;
use Kanboard\Core\Translator;

class Plugin extends Base
{
    public function initialize()
    {
        // Model
        $this->hook->on('model:subtask:creation:prepare', array($this, 'beforeSave'));
        $this->hook->on('model:subtask:modification:prepare', array($this, 'beforeSave'));

        // Forms
        $this->template->hook->attach('template:subtask:form:create', 'SubtaskDescription:subtask/form');
        $this->template->hook->attach('template:subtask:form:edit', 'SubtaskDescription:subtask/form');

        $this->template->setTemplateOverride('subtask_converter/show', 'SubtaskDescription:subtask_converter/show');

        // Task Details
        $this->template->hook->attach('template:subtask:table:header:before-timetracking', 'SubtaskDescription:subtask/table_header');
        $this->template->hook->attach('template:subtask:table:rows', 'SubtaskDescription:subtask/table_rows');

        // Dashboard - Removed after 1.0.41
        $wasmaster = APP_VERSION;

        if (strpos(APP_VERSION, 'master') !== false && file_exists('ChangeLog')) {
            $wasmaster = trim(file_get_contents('ChangeLog', false, null, 8, 6), ' ');
        }

        if (version_compare($wasmaster, '1.0.40') <= 0) {
            $this->template->hook->attach('template:dashboard:subtasks:header:before-timetracking', 'SubtaskDescription:subtask/table_header');
            $this->template->hook->attach('template:dashboard:subtasks:rows', 'SubtaskDescription:subtask/table_rows');
        }

        // Board Tooltip
        $this->template->hook->attach('template:board:tooltip:subtasks:header:before-assignee', 'SubtaskDescription:subtask/table_header');
        $this->template->hook->attach('template:board:tooltip:subtasks:rows', 'SubtaskDescription:subtask/table_rows');
    }

    public function onStartup()
    {
        Translator::load($this->languageModel->getCurrentLanguage(), __DIR__.'/Locale');
    }

    public function beforeSave(array &$values)
    {
        $this->helper->model->resetFields($values, array('due_description'));
    }

    public function getPluginName()
    {
        return 'SubtaskDescription';
    }

    public function getPluginDescription()
    {
        return t('Adds a markdown capable description field to subtasks.');
    }

    public function getPluginAuthor()
    {
        return 'Shaun Fong, Alfred Bühler';
    }

    public function getPluginVersion()
    {
        return '1.1.3';
    }

    public function getPluginHomepage()
    {
        return 'https://codeberg.org/abu/SubtaskDescription';
    }
}
