# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

- Nothing so far.

---
## [v1.1.3](https://codeberg.org/abu/SubtaskDescription/releases/tag/v1.1.3) - 2023-08-16

### Fixed

- [#1](https://codeberg.org/abu/SubtaskDescription/issues/1) Content loss when converting subtask to task.

## [v1.1.2](https://codeberg.org/abu/SubtaskDescription/releases/tag/v1.1.2) - 2023-08-15

### Changed

- Migrated from the original SubtaskDescription Plugin by Shaun Fong.  
Plugin has a new [home](https://codeberg.org/abu/SubtaskDescription).

---
