<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude([
        'Template'
    ])
    ->in(__DIR__)
;
$config = new PhpCsFixer\Config();
$config
    ->setUsingCache(true)
    ->setFinder($finder)
;
return $config;
